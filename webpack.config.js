var path = require("path");

module.exports = {
  entry: path.join(__dirname, "build/index.js"),
  output: {
    path: path.join(__dirname, "public"),
    filename: "index.js"
  },
  devtool: "eval-source-map",
  resolve: {
    extensions: [".js"]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader"
          }
        ]
      },
    ]
  },
  performance: {
    hints: false
  }
};